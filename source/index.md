---
title: XV API Reference

language_tabs:
  - shell : cURL

toc_footers:
  - <a href='http://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - users
  - favorites
  - checklist
  - messaging
  - articles
  - inspirations
  - directories
  - vendors
  - promotions
  - errors

search: true
---

# Introduction

```shell
curl "http://xvapp.com/api/users/user123/password"
  -X PUT
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"oldPassword":"secret","newPassword":"topsecret"}'
```

Use these endpoints to retrieve and persist data for the XV mobile app.  All endpoints require `Content-Type: application/json` in the header.

### Staging (Development)
The domain for development is [https://xv-staging.herokuapp.com](https://xv-staging.herokuapp.com). All the API paths will be relative to this for current development.

# Authentication

Each request to an endpoint will require an access token.  This token is obtained from a successful [login](#login) or [registration](#register).

It needs to be present in the `Authorization` header for each request.

<aside class="notice">
  Currently, the token never expires.
</aside>

## JSON Web Token

The access token in the `Authorization` header will be a [JSON Web Token (JWT)](http://jwt.io/).  The token will be created by the server and is made up of 3 parts: header, payload, and verify signature.  

<aside class="notice">
  The client does not have to do any of this. The client will pass the token it obtained from <a href="#login">login</a> or <a href="#register">registration</a>.  If it's expired (received a 401 Unauthorized) then it will need to instruct the user to log in to create a new token.
</aside>

### Header

```json
{
  "typ": "JWT",
  "alg": "HS256"
}
```

|Parameter|Type|Description|
|---------|----|-----------|
|typ|String|The type of token: 'JWT'|
|alg|String|The alogirthm used to hash the token. `HS256` (HMAC-SHA256) is currently implemented|

### Payload

```json
{
  "firstName": "John",
  "lastName": "Doe",
  "roles": ["user"],
  "iat": 1443413391,
  "exp": 1443499791,
  "iss": "XVWeb",
  "sub": "5608bd8f1cd2353a61ad81ba"
}
```

|Parameter|Type|Description|
|---------|----|-----------|
|firstName|String|First name of user|
|lastName|String|Last name of user|
|roles|String|An array of roles for the user|
|iat|Number|**(Issued At) Claim** - NumericDate representation of when the JWT was issued|
|exp|Number|**(Expiration Time) Claim** - NumericDate representation of when JWT expires|
|iss|String|The issuer of the token|
|sub|String|The subject of the token, which is the userId|

### Verify Signature

```js
key = 'secretkey'
unsignedToken = encodeBase64(header) + '.' + encodeBase64(payload)
signature = HMAC-SHA256(key, unsignedToken)
```
This is the HMAC-SHA256 of a secret key and base64 encoded header and payload.
