# Errors

The XV API uses the following error codes:

Error Code | Meaning
---------- | -------
400 | Bad Request -- Incorrect or invalid parameter
401 | Unauthorized -- Your access token is missing or expired
403 | Forbidden -- Inaccessible area
404 | Not Found -- A resource is not found
405 | Method Not Allowed -- Can't call this method
406 | Not Acceptable -- You requested a format that isn't json
409 | Conflict -- A resource already exists
410 | Gone -- The resource is no longer available
500 | Internal Server Error -- We had a problem with our server. Try again later.
503 | Service Unavailable -- Temporarially offline for maintanance. Please try again later.
