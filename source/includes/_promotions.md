# Promotions

## Get All Promotions

```shell
curl "http://xvapp.com/api/promotions"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```
> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "url": String,
    "image": String,
    "updatedAt": Date,
    "createdAt": Date
  }
]
```

Get all promotions sorted by most recent as default.  The default `limit` is **25**.  Currently, does not support setting `limit` through query string.

### HTTP REQUEST

`GET /api/promotions HTTP/1.1`
