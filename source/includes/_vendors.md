# Vendors

## Get Vendors by Directory Id

```shell
curl "http://xvapp.com/api/directories/dir123/vendors?search=nails&limit=25&skip=50&loc=-177.123,32.321"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "name": String,
    "images": [String],
    "distance": String
  }
]
```

Get all vendors that belong to the specified directory id.  Use `limit` and `skip` parameters to optimize query results.  By default, the `limit` size is 25 if not specified.

<aside class="notice">
  When making a request with the <code>loc</code> query string, the results with have an additional <code>distance</code> field.  Otherwise, the <code>distance</code> field will not be present.  <code>Distance</code> is in miles.
</aside>

### HTTP REQUEST

`GET /api/directories/:id/vendors?search=nails&limit=25&skip=25&loc=-177.123,32.321 HTTP/1.1`

### URL PARAMETERS

|Parameter|Type|Description|
|---------|----|-----------|
|id|String|The id of the directory|
|search|String|Search the name of vendors|
|limit|Number|The size of results to return. Default is 25 if not specified|
|skip|Number|Skips the specified amount of results|
|loc|Point|Comma delimited coordinate value: `lng,lat`|

## Get Vendor by Id

```shell
curl "http://xvapp.com/api/vendors/:id"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
{
  "id": String,
  "name": String,
  "email": String,
  "description": String,
  "address1": String,
  "address2": String,
  "city": String,
  "state": String,
  "zip": String,
  "location": {
    "lat": Number,
    "lng": Number
  },
  "directories": [
    {
      "id": String,
      "name": String
    }
  ],
  "phone": String,
  "url": String,
  "images": [String],
  "facebook": String,
  "updatedAt": Date,
  "createdAt": Date
}
```

Get vendor by id.

### HTTP REQUEST

`GET /api/vendors/:id HTTP/1.1`

### URL PARAMETERS

|Parameter|Type|Description|
|---------|----|-----------|
|id|String|The id of the vendor|
