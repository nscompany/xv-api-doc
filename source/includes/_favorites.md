# Favorites

## Add a New Favorite

```shell
curl "http://xvapp.com/api/users/user123/favorites"
  -X POST
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"type":"inspiration","url":"http://xvapp.com/api/inspirations/5610dc7a98b8ba4458b02f58"}'
```

> `Response: HTTP/1.1 201`

```json
{
  "id": String,
  "name": String,
  "type": String,
  "url": String,
  "createdAt": Date
}
```

Add a new favorite item to the user's favorites collection.l

### HTTP REQUEST

`POST /api/users/:userId/favorites HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User id|

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|name|String|true|A friendly name to use for displaying.|
|type|String|true|The type of favorite item (ex: inspiration, article, message)|
|url|String|true|Url to the resource.|

## Get All Favorites

```shell
curl "http://xvapp.com/api/users/user123/favorites"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 201`

```json
[
  {
    "id": String,
    "name": String,
    "type": String,
    "url": String,
    "createdAt": Date
  }
]
```

Get all user's favorites.

### HTTP REQUEST

`GET /api/users/:userId/favorites HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User id|

## Remove Favorite

```shell
curl "http://xvapp.com/api/users/user123/favorites/fav123"
  -X DELETE
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

Removes a favorite item from user's favorite collection.

### HTTP REQUEST

`DELETE /api/users/:userId/favorites/:favoriteId HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User id|
|favoriteId|String|true|Id of favorite item to be removed.|
