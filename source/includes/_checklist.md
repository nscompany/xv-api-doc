#Checklist

## Add Checklist Item

```shell
curl "http://xvapp.com/api/users/user123/checklists/checklist123/items"
  -X POST
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"name":"Get nails done","done":false,"tag":"nails","articleId":"5610dc7a98b8ba4458b02f58","notes":["Color theme black and yellow","Get patterns on toes"]}'
```

> `Response: HTTP/1.1 201`

```json
{
  "id": String,
  "name": String,
  "done": Boolean,
  "removed": Boolean,
  "tag": String,
  "articleId": String,
  "updatedAt": Date,
  "createdAt": Date,
  "notes": [String]
}
```

Add a new checklist item to a checklist.  

### HTTP REQUEST

`POST /api/users/:userId/checklists/:checklistId/items HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User's id|
|checklistId|String|true|Id of checklist where the item is being added|

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|name|String|true|Name of checklist item|
|done|Boolean|false|Mark the checklist item as done.  Default is `false`.|
|tag|String|false|The tag name that corresponds to an inspiration category.|
|articleId|String|false|The id of the article the item is related to.|
|notes|Array|false|Strings that represent notes for the item.|

## Get Checklists

```shell
curl "http://xvapp.com/api/users/user123/checklists"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "name": String,
    "updatedAt": Date
  }
]
```

Gets the user's to-do checklists.  This will return the checklist _categories_.

### HTTP REQUEST

`GET /api/users/:id/checklists HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|id|String|true|User's id|

## Get Checklist by Id

```shell
curl "http://xvapp.com/api/users/user123/checklists/checklist123"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
{
  "id": String,
  "name": String,
  "updatedAt": Date,
  "items": [
    {
      "id": String,
      "name": String,
      "done": Boolean,
      "removed": Boolean,
      "tag": String,
      "articleId": String,
      "updatedAt": Date,
      "createdAt": Date,
      "notes": [String]
    }
  ]
}
```

Get a specific checklist with checklist items.

### HTTP REQUEST

`GET /api/users/:userId/checklists/:checklistId HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User's id|
|checklistId|String|true|Checklist id|

## Update Checklist Item by Id

```shell
curl "http://xvapp.com/api/users/user123/checklists/checklist123/items/item123"
  -X PUT
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"name":"Get hair done","done":true,"removed":false,"notes":["Dye hair","Curl hair"]}'
```

> `Response: HTTP/1.1 200`

```json
{
  "id": String,
  "name": String,
  "done": Boolean,
  "removed": Boolean,
  "tag": String,
  "articleId": String,
  "updatedAt": Date,
  "createdAt": Date,
  "notes": [String]
}
```

Update a checklist item.

### HTTP REQUEST

`PUT /api/users/:userId/checklists/:checklistId/items/:itemId HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|User's id|
|checklistId|String|true|Checklist id|
|itemId|String|true|Checklist item id|

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|name|String|true|The name of the checklist item|
|done|Boolean|true|Is checklist item done|
|removed|Boolean|true|Is checklist item removed|
|notes|Array|true|Strings of notes for the checklist item|
