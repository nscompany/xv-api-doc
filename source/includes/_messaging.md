# Messaging

## Get All Threads

```shell
curl "http://xvapp.com/api/users/user123/threads"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
[
  {
    "recipientId": String,
    "recipientEmail": String,
    "recipientName": String,
    "lastMsg": String,
    "newMsgCount": Number,
    "updatedAt": Date,
    "createdAt": Date
  }
]
```

Get all message threads that the user created.  Default sort order is by recently updated thread.

### HTTP REQUEST

`GET /api/users/:userId/threads HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|Id of user the threads belong to|

## Get Messages

```shell
curl "http://xvapp.com/api/users/user123/threads/thread123/messages"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "threadId": String,
    "sender": {
      "id": String,
      "email": String,
      "name": String,
      "image": String
    },
    "message": String,
    "receivedAt": Date,
    "createdAt": Date
  }
]
```

Get all messages in the specified thread.  Results are sorted by most recently sent messages.

### HTTP REQUEST

`GET /api/users/:userId/threads/:threadId/messages HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|Id of user the thread belong to|
|threadId|String|true|Id of the thread the messages belong to|

## Send a Message

```shell
curl "http://xvapp.com/api/users/user123/threads"
  -X POST
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"to":"myvendor@gmail.com","message":"Hello!"}'
```

> `Response: HTTP/1.1 201`

```json
{
  "id": String,
  "threadId": String,
  "sender": {
    "id": String,
    "email": String,
    "name": String,
    "image": String
  },
  "message": String,
  "receivedAt" Date,
  "createdAt": Date
}
```

Send a message to a vendor or quince madrina.  The `to` field is abstracted to an email.  This email will be looked up in the db to retrieve the recipient then the email message will be sent.  If a thread already exists, it will post a new message to the thread and return HTTP status `200`.  If a thread does not exist, it will create a new thread and return HTTP status `201`.

### HTTP REQUEST

`POST /api/users/:userId/threads HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|userId|String|true|Id of user the message is being sent from.|

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|to|String|true|Email of vendor/user the intended message is for.|
|message|String|true|The message body.|
