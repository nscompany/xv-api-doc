# Articles

## Get All Articles

```shell
curl "http://xvapp.com/api/articles?limit=30&skip=60"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "title": String,
    "content": String,
    "updatedAt": Date,
    "createdAt": Date,
    "images": Array
  }
]
```

Get all articles sorted by most recent as default.  The default limit is 25 but this can be adjusted by specifying the `limit` parameter.

### HTTP REQUEST

`GET /api/articles?limit=30&skip=60 HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|limit|Number|false|The amount of items to return in a single call.|
|skip|Number|false|Skip a specified number of search results.|

## Get Article by Id

```shell
curl "http://xvapp.com/api/articles/5610dc7a98b8ba4458b02f58"
  -X GET
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```

> `Response: HTTP/1.1 200`

```json
{
  "id": String,
  "title": String,
  "content": String,
  "updatedAt": Date,
  "createdAt": Date,
  "images": Array
}
```

Gets an article by its id.

### HTTP REQUEST

`GET /api/articles/:id HTTP/1.1`

### URL PARAMETER

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|id|String|true|Id of the article|
