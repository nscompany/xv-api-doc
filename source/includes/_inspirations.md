# Inspirations

## Get All Inspirations

```shell
curl "http://xvapp.com/api/inspirations?search=dress&filter=cake&limit=10&skip=10&loc=-177.123,32.321"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```
> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "title": String,
    "image": String,
    "category": String,
    "updatedAt": Date,
    "createdAt": Date
  }
]
```

Get all inspirations. The default result set returns all inspirations ordered by most recent.

<aside class="notice">
  Apps will want to specify the <code>limit</code> and <code>skip</code> values for better performance. The results are limited to 25 if <code>limit</code> is not specified.  <code>Skip</code> will be needed to page properly.
</aside>

### HTTP REQUEST

`GET /api/inspirations HTTP/1.1`

### URL PARAMETERS

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|search|String|false|Search the title of inspirations|
|filter|String|false|Name of category to filter by|
|limit|Number|false|Limit the amount of results returned. If not specified, it will return all.|
|skip|Number|false|Skip a specified number of search results.|
|loc|String|false|Comma delimited longitude and latitude value: `loc=-177.123,32.321`. When this field is used, the results are sorted by nearest vendor.|

## Get Inspiration by Id

```shell
curl "http://xvapp.com/api/inspirations/ins123"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```
> `Response: HTTP/1.1 200`

```json
{
  "id": String,
  "title": String,
  "image": String,
  "category": String,
  "updatedAt": Date,
  "createdAt": Date,
  "vendors": [
    {
      "id": String,
      "name": String,
      "address1": String,
      "address2": String,
      "city": String,
      "state": String,
      "zip": String,
      "url": String,
      "location": {
        "lat": Number,
        "lng": Number
      }
    }
  ]
}
```

Get a particular inspiration by id.

### HTTP REQUEST

`GET /api/inspirations/:id HTTP/1.1`

### URL PARAMETERS

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|id|String|true|The id of the inspiration|

## Get Inspiration Categories

```shell
curl "http://xvapp.com/api/inspirations/categories"
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```
> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "name": String,
    "updatedAt": Date,
    "createdAt": Date
  }
]
```

Get all categories that an inspiration can belong to.  These are also used to `filter` inspirations in [Get All Inspirations](#get-all-inspirations).  Although there are meta data, currently, only the `name` field is used.

### HTTP REQUEST

`GET /api/inspirations/categories HTTP/1.1`
