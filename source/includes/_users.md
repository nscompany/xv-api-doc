# Users

### Supported Images

The API supports base64 encoded image or url to the image resource for image fields.

- **Base64**: Base64 encoded image with header.
  - Format: `data:{mimetype};base64,{blob}`
    - **mimetype**: Mime type of the image. (_Ex: image/png, image/jpg_)
    - **blob**: Base64 encoded image blob.
  - _Ex:_ `data:image/png;base64,myImageBase64EncodedValue`
- **Url**: Fully qualified url to the image (no relative paths)
  - _Ex:_ `http://www.facebook.com/myProfile/image.png`

## Register

```shell
curl "http://xvapp.com/api/users"
  -X POST
  -H "Content-Type: application/json"
  -d '{"email":"jdoe@gmail.com","password":"secret","provider":"local","firstName":"John","lastName":"Doe","birthdate":"1984-10-15T00:00:00.000Z","eventDate":"2016-10-15T12:00:00.000Z","location":"Irvine, CA","image":"http://s3.amazon.com/user.png"}'
```

> `Response: HTTP/1.1 201`

```json
{
  "id": String,
  "email": String,
  "firstName": String,
  "lastName": String,
  "birthdate": Date,
  "eventDate": Date,
  "location": String,
  "image": String,
  "token": String
}
```

Register a new user either with `local` app db or `facebook`.

### HTTP REQUEST

`POST /api/users HTTP/1.1`

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|email|String|true|Email for the user|
|password|String|false|Not required if signing up with Facebook|
|provider|String|true|`local` or `facebook`|
|externalId|String|false|Facebook Id|
|firstName|String|true|User's first name|
|lastName|String|false|User's last name|
|birthdate|Date|false|User's birthdate|
|eventDate|Date|false|User's quiceañera date|
|location|String|false|Location (city/state) of the user|
|image|String|false|Url or base64 encoded image. See <a href="#users">Supported Images</a>.

## Login

```shell
curl "http://xvapp.com/api/users/login"
  -X POST
  -H "Content-Type: application/json"
  -d '{"email":"jdoe@gmail.com","password":"secret","provider":"local"}'
```

> `Response: HTTP/1.1 201`

```json
{
  "id": String,
  "email": String,
  "firstName": String,
  "lastName": String,
  "birthdate": Date,
  "eventDate": Date,
  "location": String,
  "image": String,
  "token": String
}
```

Login with email/password or facebook.

### HTTP REQUEST

`POST /api/users/login HTTP/1.1`

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|email|String|true|Email of user|
|password|String|false|Not required if logging in with Facebook|
|provider|String|true|`local` or `facebook`|

## Update Profile

```shell
curl "http://xvapp.com/api/users/user123"
  -X PUT
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"firstName":"Johnny","lastName":"Doe","birthdate":"1984-10-16T00:00:00.000Z","eventDate":"2016-10-16T12:00:00.000Z","location":"Irvine, CA","image":"http://s3.amazon.com/user.png"}'

```
> `Response: HTTP/1.1 200`

Update user's profile.  

If password needs changing, see [Change Password](#change-password).

### HTTP REQUEST

`PUT /api/users/:id HTTP/1.1`

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|firstName|String|true|User's first name|
|lastName|String|true|User's last name|
|birthdate|Date|false|User's birthdate|
|eventDate|Date|false|User's quinceañera date|
|location|String|false|Location (city/state) of the user|
|image|String|false|Url or base64 encoded image.  See <a href="#users">Supported Images</a>.

## Change Password

```shell
curl "http://xvapp.com/api/users/user123/password"
  -X PUT
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"oldPassword":"secret","newPassword":"topsecret"}'
```

> `Response: HTTP/1.1 200`

Change user's password.  Server will confirm the old password prior to updating with the new.

### HTTP REQUEST

`PUT /api/users/:id/password HTTP/1.1`

### URL PARAMETERS

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|id|String|true|User id|

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|oldPassword|String|true|The user's current password|
|newPassword|String|true|The user's password to be updated|

## Reset Password

```shell
curl "http://xvapp.com/api/users/resetpassword"
  -X PUT
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
  -d '{"email":"jdoe@gmail.com"}'
```

> `Response: HTTP/1.1 200`

Reset user's current password. The user will be looked up by the `email` posted in the body of the request and a temporary password will be issued via email notification. User can then login and update their password.

### HTTP REQUEST

`POST /api/users/resetpassword HTTP/1.1`

### BODY

|Parameter|Type|Required|Description|
|---------|----|--------|-----------|
|email|String|true|Email of user|
