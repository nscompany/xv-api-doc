# Directories

## Get All Directories

```shell
curl "http://xvapp.com/api/directories
  -H "Content-Type: application/json"
  -H "Authorization: Bearer <ACCESS_TOKEN>"
```
> `Response: HTTP/1.1 200`

```json
[
  {
    "id": String,
    "name": String,
    "image": String,
    "order": Number,
    "createdAt": Date
  }
]
```

Directories are categories for vendors.  This will return all directories in this order: _Photography_, _Venues_, the rest in alphabetical order. The default should be sorted by the `order` field.

To get vendors from a directory, see [Get Vendors By Directory Id](#get-vendors-by-directory-id).

### HTTP REQUEST

`GET /api/directories HTTP/1.1`
